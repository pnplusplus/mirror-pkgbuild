#!/usr/bin/env bash 
#
find x86_64 -type d -not -name "x86_64" -not -name "epson-inkjet-printer-201310w" -not -name "epson-printer-utility" -not -name "celluloid-gtk3" -not -name "baobab-gtk3" -exec rm -rfv {} +
cd x86_64
yay -G --aur 7-zip-full elementary-xfce-icons rar rhythmbox-plugin-alternative-toolbar xfce4-zorinappgridlite-plugin xfce-theme-greybird zorin-appgrid-lite mint-themes-git
cd ..
find x86_64 -type f -not -name PKGBUILD -not -name LICENSE -not -name "*.install" -not -name "*.desktop" -not -name "*.patch" -not -name "*.diff" -not -name "*.service" -not -name "rar.1" -not -name "AVASYSPL.en.txt"  -delete
find x86_64 -type d -name .git -exec rm -rfv {} +
